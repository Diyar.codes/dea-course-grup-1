class Player {
    constructor() {
        this._username = "";
    }

    generateToken() {
        const random = ~~[Math.random() * 10000];
        const token = this.username + random.toString();
        return token;
    }

    generateTokenVVIP() {
        const token = this.username
        return token;
    }

    // setter method
    set username(_username) {
        return this._username = _username;
    }

    // getter method
    get username() {
        return this._username;
    }

    get register() {
        sessionStorage.setItem('token', this.generateToken());
        sessionStorage.setItem('level', 1);
        registerForm.style.display = "none";
        logoutForm.style.display = "block";

        setTimeout(function() {
            location.href = "#start"
        }, 500);
    }

    get registerVVIP() {
        sessionStorage.setItem('token', this.generateTokenVVIP());
        sessionStorage.setItem('level', 1);
        registerForm.style.display = "none";
        logoutForm.style.display = "block";

        setTimeout(function() {
            location.href = "#start"
        }, 500);
    }

    get logout() {
        sessionStorage.removeItem('token');
        sessionStorage.removeItem('level');
        location.reload();
    }
}