// declaring element
const username = document.getElementById("username");
const registerForm = document.getElementById("registerForm");
const logoutForm = document.getElementById("logoutForm");
const startSection = document.getElementById("start");

const rewardImage = document.getElementById("imgReward");
const gamewrapper = document.getElementById("gamewrapper");

const player = new Player();
let default_option = ['😍', '🤣', '😮']; // default sticker

let resultBoxs; // box diisi dari function register dan onload

function dice() {
    let gacha = [];
    for (let i = 0; i < resultBoxs.length; i++) {
        const roll = default_option[~~(Math.random() * default_option.length)];
        gacha.push(roll);
    }

    return gacha;
}

function reward() {
    fetch('https://zoo-animal-api.herokuapp.com/animals/rand')
        .then(x => x.json())
        .then(result => {
            let text = document.createElement('h1')
            text.textContent = result.name

            let img = new Image(200, 200)
            img.src = result.image_link

            rewardImage.appendChild(img)
            rewardImage.appendChild(text)
        })
}

function winner(match, resultBoxs) {
    let one = match.splice(-resultBoxs, resultBoxs); // ambil beberapa data terakhir, misal level 1 berati ambil 3 data terakhir || level 2 berati ambil 4 data terakhir
    if(one.length != 1) { // jika data tersisa hanya 1 setelah dicocokkan maka winner
        if(one[0].textContent === one[1].textContent) { // cocokkan data ke 0 dan 1, jika cocok maka akan di shift(diambil data pertama) dan next data ke 1 jadi 0 & 2 jadi 1 sampai kecocokan habis
            one.shift(); // jika data cocok maka buang data pertama array
            winner(one, resultBoxs - 1); // lakukan kembali hingga data yang match tinggal 1
        } else {
            location.href = "#start";
        }
    } else {
        reward();
        location.href = "#reward";
    }
}

function start() {
    let match = [];
    if(sessionStorage.getItem('token') == 'superman') { // jika token username maka otomatis menang
        const rolling = setInterval(function() {
            const random = default_option[Math.floor(Math.random() * 3)]; // pilih angka random dari 1 - 3 untuk mengambil key dari array sticker
            for (var i = 1; i <= resultBoxs.length; i++) { // generate variable baru sesuai level, ex level 1 : box1, box2, box3 || level 2 : box1, box2, box3, box4
                this["box"+i].textContent = random; // pasang key sticker dari var random
                match.push(this["box"+i]);
            }
        }, 70)

        setTimeout(function() {
            clearInterval(rolling);
            winner(match, resultBoxs.length); // cek key array kemenangan apakah sama
        }, 2500);
    } else { // jika token username biasa
        const rolling = setInterval(function() {
            const result = dice(); // ambil data function dice() di dalam array sticker
            for (var i = 1; i <= resultBoxs.length; i++) {
                this["box"+i].textContent = result[Math.floor(Math.random() * resultBoxs.length)]; // pasang semua key array sticker berdasarkan jumlah sticker dan level
                match.push(this["box"+i]);
            }
        }, 70)

        setTimeout(function() {
            clearInterval(rolling);
            winner(match, resultBoxs.length); // cek key array kemenangan apakah sama
        }, 2500);
    }
}

onload = function() {
    const token = sessionStorage.getItem('token');
    const level = sessionStorage.getItem('level');

    if(token && token !== null && level && level !== null) { // jika token & level tidak ada atau kosong
        registerForm.style.display = "none";
        logoutForm.style.display = "block";
    } else {
        registerForm.style.display = "block";
        logoutForm.style.display = "none";
    }

    location.href = "#start"; // ketika di refresh ke halaman start
    resultBoxs = box(); // isi kembali jumlah box seusai level setelah refresh
}

function box() {
    const level = sessionStorage.getItem('level');
    let box = [];
    for (let i = 0; i < parseInt(level) + 2; i++) { //pengulangan pembuatan div berdasarkan level sekarang
        var newDiv = document.createElement('div'); // buat element div
        newDiv.id = 'box' + (parseInt(i) + 1); // set id element
        newDiv.className = 'box'; // set class element
        newDiv.textContent = default_option[Math.floor(Math.random() * 3)]; // isi content dengan sticker
        gamewrapper.appendChild(newDiv); // tambah kan element div tadi ke dalam element dengan id gamewrapper
        box.push(newDiv.id); // kumpulkan semua id untuk kepentingan jumlah
    }
    return box;
}

function nextlevel() {
    const level = sessionStorage.getItem('level'); // ambil data level sekarang
    const levelNext = parseInt(level) + 1; // tambahkan 1 level
    sessionStorage.setItem("level", levelNext); // set level yang sudah ditambahkan
    location.reload(); // refresh
    location.href = "#start"; // kembail ke start
}

function register() {
    if(username.value === 'superman') { // jika username adalah superman silah kan lanjutkan ke function register vvip
        player.username = username.value;
        player.registerVVIP;
    } else {
        player.username = username.value;
        player.register;
    }

    resultBoxs = box(); // isi kembali jumlah box seusai level setelah register
}

function logout() {
    player.logout();
}